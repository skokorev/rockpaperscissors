package ru.test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class Application2 {

    public static void main(String[] args) {
        try {
            Files.write(
             Paths.get("C:\\TEST", "test.txt"),
                    "mytestbytes".getBytes(),
                    StandardOpenOption.CREATE ,
                    StandardOpenOption.WRITE ,
                    StandardOpenOption.APPEND);

            if (Files.exists(Paths.get("C:\\TEST", "test.txt")))
                System.out.println("All right! ");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
