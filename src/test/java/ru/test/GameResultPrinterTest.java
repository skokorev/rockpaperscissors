package ru.test;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

public class GameResultPrinterTest {

    @Test
    public void shouldPrintGameResult() throws IOException {
        GameResult result = new GameResult();
        result.incrementPlayer1WinCount();
        result.incrementTieCount();
        GameResultPrinter printer = new GameResultPrinter();
        assertNotNull(printer);

        PrintStream oldStream = System.out;
        try (ByteArrayOutputStream newOut = new ByteArrayOutputStream();
            PrintStream newOutPs = new PrintStream(newOut)) {
            System.setOut(newOutPs);
            printer.print(result);
            assertEquals("Player 1 wins 1 of 2 games\nPlayer 2 wins 0 of 2 games\nTie: 1 of 2 games\n", newOut.toString().replace("\r\n", "\n"));
        }
        System.setOut(oldStream);
    }
}
